
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] bool isStrongBlock = false;
   
    int health = 1;
    void OnCollisionEnter2D(Collision2D collisionInfo)
    {
        //destroy block
        if (health < 1 || !isStrongBlock) Destroy(gameObject);
        else health--;
        
    }
}
