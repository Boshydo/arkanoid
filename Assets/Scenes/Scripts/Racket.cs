using UnityEngine;

public class Racket : MonoBehaviour
{
    //movement speed 
    public float speed = 150;
    public Gamemanager gm;
    private void Update()
    {
        if (gm.gameover)
        {
            return;
        }
    }

    private void FixedUpdate()
    {
        //horizontal input
        float h = Input.GetAxisRaw("Horizontal");
        //set velocity (Movement speed * direction)
        GetComponent<Rigidbody2D>().velocity = Vector2.right * h * speed;
    }
}
