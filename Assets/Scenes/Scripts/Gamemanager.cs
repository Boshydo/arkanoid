using TMPro;
using UnityEngine;

public class Gamemanager : MonoBehaviour
{
    
    public int hp;
    public TextMeshProUGUI healthtext;
    public bool gameover;
    public TextMeshProUGUI gameovertext;
    void Start()
    {
        healthtext.text ="x " + hp;
    }
    public void lives(int life)
    {
        hp +=life;
        //check for no lives left and trigger the end of game
        
        if (hp <= 0)
        {
            hp = 0;
            Gameover();
        }
        healthtext.text = "x " + hp;
    }
    void Gameover()
    {
        gameover = true;
        gameovertext.text = "GAME OVER";

    }



}
