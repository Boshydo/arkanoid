using UnityEngine;

public class Ball : MonoBehaviour
{
    public Rigidbody2D rb;
    private bool startplay;                      
    public Transform Racket;
    public float speed = 30f;
    public Gamemanager gm;
    void Start(){
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        if (gm.gameover)
        {
            return;
        }
        // startplay is used to add force to ball when triggering the jump key 
        if (!startplay) {
            transform.position = Racket.position;
        }
        if (Input.GetButtonDown("Jump") && !startplay) {
            startplay = true;
            GetComponent<Rigidbody2D>().velocity = Vector2.up * speed;
        }
    }
    float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketWidth) {
        return (ballPos.x - racketPos.x / racketWidth);
    }
    private void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.name == "Racket")
        {
            //calculate the hit factor
            float x = hitFactor(transform.position, col.transform.position, col.collider.bounds.size.x);
            Vector2 dir = new Vector2(x, 1).normalized;
            GetComponent<Rigidbody2D>().velocity = dir * speed;
        } 
    }
    private void OnTriggerEnter2D(Collider2D Other)
    {
        if (Other.CompareTag("bottom"))
        {
            Debug.Log("the ball hit the bottom of the screen");
            rb.velocity = Vector2.zero;
            startplay = false;
            gm.lives(-1);

        }
    }



}

